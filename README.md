# markov

A multithreaded character-based markov chain generator, inspired by [Yoav Goldberg's post](https://nbviewer.jupyter.org/gist/yoavg/d76121dfde2618422139).

Some examples of generated text:

**/r/nosleep**:

>I have called the CDC multiple times but every time it comes back in view the 
>image is less sharp, until it exists as a pulsing dark pulsing all together and 
>it's so huge, so huge and you can feel it go, but there's no point now. I 
>thought he'd come early. I found out later that the coroner and police had 
>discovered, and my friend wouldn't dream of hurting you. We want to help you 
>learn to control your breathing manually. This is normal. 
>
>We know from other communication frequencies worked. After several unsuccessful 
>hours, I went about determining our position. Toiling for an embarrassing 
>amount of time, I arrived at my conclusion. It was unmistakable - about 47 
>degrees west. That was about an hour ago, however, I did get into it. And I 
>wrote down everything you experience. Build some data. Maybe we can map your 
>psychic wound."
>
>Wow. "Sounds like a plan."
>
>If he was here, at least I could do it for Julie. She needed a loving family

---

>I know there was someone down here and then I walked all the way downstairs. A 
>part of me really hoped to see my dad sitting by my bedside. When he saw that I 
>was right. The city had gone from ancient to modern in only sixty seconds. I 
>didn't see him anywhere, but I did see some weird animals - two bluish giraffes 
>- walking by. The low resolution made it near impossible to tell if they were 
>buried, or if it had never been close with her.  Instead, Shirley began to sing.
>
>We eat their teeth
>
>And eat their bones
>
>And slit their throats inside their homes
>
>We sing this song
>
>And when we're done
>
>We'll go to hell and have more fun.
>
>She sang it all the time.   I'll report back with any new developments" do you 
>not understand?!"
>
>"Look... Mrs. Goodrim, I'm just here to do a job. Whether you can prove to me 
>that one, you have proof that he's trying to make out anybody from where I sat. 
>I just waited for the eventual stomp down the hallway. But it never came, and I 
>fell in line behind Morgan, relieved. 
>
>
>"What's wrong, little piggie?" Carolyn was standing behind me and I was half 
>hanging out of the car. I think she's going to stop."
>
>"You have to get me out of jail.  He risked bribing an official and left a lot 
>of his plan up to chance to get me out.  David just didn't want to be in this 
>creepy man's house. I didn't want to be a skeptic either.  I had to arrange the 
>funeral alone.  Sleeping was impossible.  My wife was just a statue now, going 
>through the water. The still surface began to churn with the enigmatic energy. 
>It took my scattered mind a long while to realize that this was a simple 
>technical malfunction. None were met with success, except one.

**/r/dndgreentext:**

That time my players obsessed over the gay couple for way too long

>\> be me, first time DM
>
>\> be not me, fist time party
>
>\>  a little less than a year ago on Roll20, don't remember their name, but it's 
>a symbol of a demon prince.
>
>The demon prince is not just any demon prince, but a horny demon prince! 
>(Earlier in the campaign.
>
>\>Human Barbarian, cool guy, has a redneck southern accent, the party likes 
>signing deals.
>
>\>Be me, DM
>
>\>3rd campaign with group, it's been tradition for party to sign contracts with 
>devils
>
>\>Introduce Orthon, Devil bounty hunter
>
>\>Speaks with very bad southern accent
>
>\>"Yall don't want to head into that there temple, demons have been lovin it"
>
>\>Why are you warning us about it?
>
>\>"Seems like I don't want to die when I go after 'em, so maybe your party helps 
>me out?"
>
>\>Party decides to sleep on the roof.
>
>He gets hypnotized by the master vampire, goes inside and kills the girl.

---

>A few choice moments in my Call of Cthulhu game so far.
>
>I'm the GM. The setting is 1922 New York. The party consists of Two Tabaxi, one 
>a paladin, one a bard. An animate suit of armour Warlock. A halfling druid and 
>a dragonborn fighter\] look for a way to get in, and there are windows, but 
>they're... A bit too small. 
>
>Sorcerer, OOC: Are you trying to say that she's too fat?!
>
>DM: No! She has... Wide, egg laying hips
>
> DM: You \[dragonborn fighter with a shitton of charisma.
>
>\>rest of the party consists of Chaos Worshipers.
> 
>
>\>Be not me, Lord Fireball the Mighty. Aka Erasmus, fallen wizard sworn to 
>Nurgle.  
>  
>\>  
>\>  
>\>Glop, a seer and sorcerer, also in service to Nurgle.
>
>
>
>\>  
>\>One the very first day of their invasion of Bretonnia I had Tzeentch Cultists 
>bother the gang.  
>  
>\>  
>\>  
>\>There is what appears to be a blooming flower
>
>\>"cute"

---

**[Internet Argument Corpus 1.1](https://nlds.soe.ucsc.edu/iac)**:

>Evolution is falsifiable. If a fossil were found in a rock later that dated 
>back to a radically different from what you said that I said.
> 
> I never said there needed to be a God, I said I believe in CREATIONISM! lol 
>emoticon_xbouncer emoticon_xbouncer emoticon_xhoho emoticon_xbouncer 
>emoticon_xbouncer
>
>----------
>
> 
> 
> I can't believe this. You say, "That is a pretty crappy trade. Your increase 
>in personal protection and thus make a greater range of habitats possible. 
>Feathers also offer increased protection from such an end due to the blood 
>covering of Jesus sacrifice for us. I seek to be more like Christ because that 
>is when some people affect other people adversely. But that doesnot seem to 
>matter to the creationists.
>
>----------
>In recent years, the United Nations (with an assist from the media), did a 
>little too vociferous a job smearing anyone who disagrees with you (as do most 
>legal experts). They disagree with your naive acceptance of ancient mythology 
>is a historical record of the events, you have made assertions that can be 
>checked. For example, if an airplane is swiftly losing altitude, and the oxygen 
>masks drop, an adult must give themselves oxygen first, then to the child. 
>Despite our best intentions, if we forget to care for the child. This is not an 
>answer, but a quote to get your opinion on it... as I don't know as much about 
>human life as we know now, and the church hadn't made an official doctrine 
>until later. 
> 
> The egg came before the chicken.
> 
> As I've stated earlier, to go CCW with the thought of being hero, is 
>unrealistic. That is a awesome , one hundreth of a second descison , to have to 
>make to pull a gun out to save someone else, because one never knows the 
>situation that it created. The Jews could not fight back in the ghettos, 
>resistance fighters all over Europe had weapons smuggled by US commandos, etc. 
> 
> Hitler did enact strict guncontrol in every country the Germany army invaded. 
>The result was what you saw in Bosnia. Government forces driving around unarmed 
>villages and cities rounding up people.

---

>I was listening to the brady bunch, pistol grips are not for shooting from the 
>hip, it's ergonomics. Just about every target rifle ever made, including 
>bolt-actions, have pistol grips. It allows more precise control over trigger 
>actuation. Besides, try holding something vertical at your hip with your hand 
>and see how comfortable it feels, the traditional straight grip is more 
>preferable if your were hip shooting. 
> 
> Guess I'm done with my bit of a rant, for now anyways.
>
>----------
>so Jesus supported ALL of the punishments that were in the book of numbers and 
>leviticus. i hope you realize that there are flawed passages such as saying the 
>world is the center of the solar system?*
> 
> Do you think that the Supreme court ruled on the 3/4 compromise that was 
>legislated. Therefore, Parcum is ignorant of American History. Quite frankly, 
>history is my forte. I've been taking a break.
>
>----------
>
> This point is way too unimportant to belabor further, but I will make one more 
>point. The thread title is not really a theory at all rather a question, which 
>I am sure you will back this up as well, when somebody tries to impose "baby" 
>laws on the fetus, despite the reality that is, is different. 
> 
> 200 years ago, the best place to read about the Paluxy Man tracks is Glen 
>Kuban's articles on the Paluxy tracks. The Texas Dinosaur/"Man Track" 
>Controversy.
>
>----------
>
> 
> I agree.
> 
> "This logic is much like saying something ridiculous.

---

>4Forums.com Political Debates and Polls -  Urban Legend.  It should be illegal. 
> 
> LovetheSouth
>
>----------
>I obviously don't agree with yours!
> 
> That makes sense. You need guns to protect yourself.
>
>----------
>
> 
> 
> I didn't consider it wrong. I never said you were white. The fact that gun 
>haters like you should be happy that you are alive, live life day by day, set 
>goals, have fun, socialize, do somethign groundbreaking, leave your mark in 
>history, change the world, better yopurself, better others. unless, of course, 
>you want the security men to be armed. IF you want them armed, why not let law 
>abiding citizens' civil rights.
>
>----------
>
> 
> You insist science should acknowledge their presence as their lord and master 
>before this all covering evil falls away, so they must be slaves to this 
>master, to be free, seems like an incredible use of doublethink.
> 
> Christianity has traditionally been considered small-arms (rifles, shotguns, 
>and handguns). Mr. Konsen speculates that the word "arms" encompasses nearly 
>the whole of the landscape of the earth do the Tigris and Euphrates are names, 
>not places. If I built a city on the moon and named it New York, that does not 
>mean I think it has any business being forced on others. They may not share my 
>faith. And they don't have to "dug up every square inch" to search. All they 
>have to do is use any of the satellites which are also used for geological 
>surveys. The UN Inspectors, and they were experts, for there for months, and 
>the USA has been there for many more months, with much more detection 
>equipment. And yes, the airlines should allow terrorists to carry lethal 
>weapons on board a plane. I dont know if you have seen a scientist stating the 
>laws of thermodynamics) and generally the more ordered the signal, the more 
>information.
>
>----------
>
> 
> The Scots have thier own Big Mac.......
>

## Requirements:

- Linux (pthreads)
- C++-17 compiler (tested on GCC and clang)

## Instructions:

Build the project with GCC using `make`. The program consumes 2 optional preprocessor definitions:

- `ORDER`: the order of the chain, in characters
- `NUM_THREADS`: number of threads to use when generating the chain model

    make CPPFLAGS="-DORDER=15 -DNUM_THREADS=16"

or

    g++ -pthreads -DORDER=15 -DNUM_THREADS=16 markov_multi.cpp

Run the program:

    ./markov_multi filename.txt [character length per sentence] [number of sentences to generate] 
