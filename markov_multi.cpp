#include <fstream>
#include <iostream>
#include <string>
#include <map>
#include <array>
#include <random>
#include <mutex>
#include <thread>
#include <queue>
#include <condition_variable>
#include <cstring>

using namespace std;

#ifndef ORDER
#define ORDER 25
#endif
static_assert(ORDER > 0, "ORDER must be positive");

#ifndef NUM_THREADS
#define NUM_THREADS 16
#endif
static_assert(NUM_THREADS > 0, "NUM_THREADS must be positive");

typedef map<wchar_t, float> Frequencies;
typedef map<array<wchar_t, ORDER>, Frequencies> Chain;

class Producer;

class Consumer {
private:
	const int id;
	Producer &producer;
public:
	Consumer(const int id, Producer& producer);

	void run();
};

class Producer
{
private:
	mutex getWorkMut;
	mutex taskDoneMut;

	shared_ptr<Chain> intermediateResult;
	queue<Chain> threadResults;
	condition_variable workReadyCV;
	wifstream input;
	array<wchar_t, ORDER> lastChars;

	const int CHUNK_SIZE = 500000;
	int threadsComplete = 0;

	// continuously grab items from the list of completed work items and update the
	// intermediate chain with results
	void mergeIntermediates() {
		while (threadsComplete < NUM_THREADS) {
			// grab lock
			unique_lock<mutex> lck(getWorkMut);

			// cv wait if empty
			if (threadResults.empty()) {
				workReadyCV.wait(lck);
			}

			// in case the last thread ended before the main thread could mark it as done, check for threadsComplete again
			if (threadResults.empty() && threadsComplete == NUM_THREADS) {
				return;
			}

			Chain threadResult = threadResults.front();
			threadResults.pop();

			lck.unlock(); // don't need mutex on queue for merging

			for (auto i : threadResult) {
				auto maybeFreq = (*intermediateResult).find(i.first);
				if (maybeFreq != (*intermediateResult).end()) {
					Frequencies existing = maybeFreq->second;
					for (auto newFreq : i.second) {
						existing[newFreq.first] += newFreq.second;
					}
					(*intermediateResult)[i.first] = existing;
				} else {
					(*intermediateResult)[i.first] = i.second;
				}
			}
		}
	}

public:
	struct Work {
		wstring text;
		// last N characters that appeared before 'text'
		array<wchar_t, ORDER> lastChars;
	};

	class NoWork {};

	Producer(string fname) {
		input.exceptions(input.exceptions() | ios::failbit);
		input.open(fname);
		if (!input.is_open() || input.fail()) {
			throw "failed to open file " + fname;
		}
		input.imbue(locale("en_US.UTF8"));
		lastChars.fill('#');
		intermediateResult = shared_ptr<Chain>(new Chain());
	}

	shared_ptr<Work> getWork() {
		lock_guard<mutex> lck(getWorkMut);

		// throw NoWork if eof
		if (input.eof()) {
			throw NoWork{};
		}

		wstring text;
		text.resize(CHUNK_SIZE);

		// guess it works
		// https://stackoverflow.com/a/15883626/2329879
		try {
			input.read(&text[0], CHUNK_SIZE);
		} catch(ios_base::failure) {
			// ifstream::read sets eofbit&failbit if there aren't enough chars left to read.
			// check to see if we're eof, if so then ignore this
			if (!input.eof()) {
				throw;
			}
		}
		shared_ptr<Work> work(new Work{text, lastChars});

		// update history
		int charsRead = 0;
		for (int i = text.length() - ORDER; i < text.length() && i > 0; i++) {
			lastChars[charsRead] = text[i];
			charsRead++;
			if (charsRead == ORDER) {
				break;
			}
		}

		for (int j = charsRead; j < ORDER; j++) {
			lastChars[j] = '#';
		}

		return work;
	}

	void taskDone(shared_ptr<Chain> intermediateChain, int id) {
		unique_lock<mutex> lck(taskDoneMut);
		// consumer will wipe its own chain once this function completes,
		// so make a copy of it
		threadResults.push(*intermediateChain);

		cerr << ".";

		lck.unlock();
		workReadyCV.notify_one();
	}

	shared_ptr<Chain> run() {
		// spawn NUM_THREADS threads
		array<thread*, NUM_THREADS> threads;
		for (int i = 0; i < NUM_THREADS; i++) {
			Consumer c(i, *this);
			threads[i] = new thread(&Consumer::run, c);
		}

		thread merger(&Producer::mergeIntermediates, this);

		// join
		for(auto t : threads) {
			t->join();
			threadsComplete++;
			delete t;
		}
		cerr << endl;

		workReadyCV.notify_one();
		merger.join();

		// normalize result
		for(auto i : *intermediateResult) {
			float totalCount = 0;
			for(auto freqIter : i.second) {
				totalCount += freqIter.second;
			}

			for(auto freqIter : i.second) {
				float norm = freqIter.second / totalCount;
				(*intermediateResult)[i.first][freqIter.first] = norm;
			}
		}

		return intermediateResult;
	}
};


Consumer::Consumer(const int id, Producer& producer) : id(id), producer(producer) {
}

void Consumer::run() {
	// continuously accept work from the producer, shipping intermediate chain results
	// back up every time work is complete. this approach helps save memory by reducing
	// the size of the intermediate chain that each consumer has to store.
	shared_ptr<Chain> chain(new Chain());
	while (true) {
		try {
			shared_ptr<Producer::Work> work = producer.getWork();

			array<wchar_t, ORDER> history = work->lastChars;

			for(wchar_t c : work->text) {
				auto maybeFreq = chain->find(history);
				if (maybeFreq != chain->end()) {
					maybeFreq->second[c]++;
				} else {
					Frequencies freq;
					freq[c]++;
					(*chain)[history] = freq;
				}

				for (int i = 0; i < ORDER - 1; ++i)
				{
					history[i] = history[i + 1];
				}
				history[ORDER - 1] = c;
			}
			producer.taskDone(chain, id);
			chain->clear();
		} catch (Producer::NoWork) {
			producer.taskDone(chain, id);
			return;
		}
	}
}


wstring generate(shared_ptr<Chain> chain, int length) {
	// https://stackoverflow.com/a/17798317/2329879
	random_device rd;
	mt19937 mt(rd());
	uniform_real_distribution<> distr(0, 1);

	wstring output;
	array<wchar_t, ORDER> history;
	history.fill('#');

	// randomly select a starting point
	for (auto entry : *chain) {
		if (distr(mt) > 0.85) {
			history = entry.first;
			break;
		}
	}

	/*
	wcout << "starting history=";
	for(wchar_t c : history)
		wcout << c;
	wcout << endl;
	*/

	for(int i = 0; i < length; i++) {
		wchar_t nextChar;
		Frequencies nextPossible = (*chain)[history];

		// for(auto wordChr : history) {
		// 	wcout << wordChr;
		// }
		// wcout << endl;
		// for(auto j : chain[history]) {
		// 	wcout << "\t" << j.first << " " << j.second << endl;
		// }

		double x = distr(mt);
		for(auto charProb : nextPossible) {
			x -= charProb.second;
			if(x <= 0) {
				nextChar = charProb.first;
				break;
			}
		}
		for (int i = 0; i < ORDER - 1; ++i) {
			history[i] = history[i + 1];
		}
		history[ORDER - 1] = nextChar;

		output += nextChar;
	}
	return output;
}

int main(int argc, char const *argv[])
{
	bool isHelp = argc == 2 && (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0);

	if (argc < 2 || argc > 4 || isHelp) {
		cerr << "usage: " << argv[0] << " file.txt [sentence length] [number of sentences]" << endl;
		return 1;
	}
	
	int sentenceLength = 1000;
	int numSentences = 2;

	if (argc >= 3) {
		sentenceLength = atoi(argv[2]);
		if (sentenceLength <= 0) {
			cerr << "sentence length must be > 0" << endl;
			return 1;
		}
	}

	if (argc == 4) {
		numSentences = atoi(argv[3]);
		if (numSentences <= 0) {
			cerr << "num sentences must be > 0" << endl;
		}
	}

	cerr << "ORDER=" << ORDER << endl
	     << "NUM_THREADS=" << NUM_THREADS << endl;
	cerr << "Printing " << numSentences << " sentences of length " << sentenceLength << " chars" << endl;

	string fname = string(argv[1]);
	Producer prod(fname);
	shared_ptr<Chain> chain = prod.run();


	// for(auto i : nChain) {
	// 	for(auto wordChr : i.first) {
	// 		wcout << wordChr;
	// 	}
	// 	wcout << endl;
	// 	for(auto j : i.second) {
	// 		wcout << "\t" << j.first << " " << j.second << endl;
	// 	}
	// }

	for (int i = 0; i < numSentences; i++) {
		wstring gen = generate(chain, sentenceLength);
		wcout << gen << endl;
		wcout << endl << "***************" << endl << endl;
	}

	// nlohmann::json jChain(*chain);
	// cout << jChain << endl;
}
