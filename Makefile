CC=g++
CFLAGS=-O3 -pthread

.PHONY:clean

all:
	$(CC) $(CPPFLAGS) $(CFLAGS) markov_multi.cpp -o markov

markov_single:
	$(CC) $(CPPFLAGS) $(CFLAGS) markov.cpp -o markov_single

clean:
	rm markov_single markov
