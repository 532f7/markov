#include <fstream>
#include <iostream>
#include <string>
#include <map>
#include <array>
#include <random>
#include "json.hpp"

using namespace std;

const int ORDER = 25;

typedef map<wchar_t, int> Frequencies;
typedef map<wchar_t, float> NormalFrequencies;
typedef map<array<wchar_t, ORDER>, Frequencies> Chain;
typedef map<array<wchar_t, ORDER>, NormalFrequencies> NormalChain;


Chain getChain(string fname) {
	wifstream file(fname);
	if (!file.is_open()) {
		throw "failed to open file " + fname;
	}

	file.imbue(locale("en_US.UTF8"));

	Chain chain;

	wchar_t c;
	array<wchar_t, ORDER> history;
	history.fill('#');

	while (file >> noskipws >> c) {
		auto maybeFreq = chain.find(history);
		if (maybeFreq != chain.end()) {
			maybeFreq->second[c]++;
		} else {
			Frequencies freq;
			freq[c]++;
			chain[history] = freq;
		}

		for (int i = 0; i < ORDER - 1; ++i)
		{
			history[i] = history[i + 1];
		}
		history[ORDER - 1] = c;
	}

	return chain;
}

NormalChain normalize(Chain &chain) {
	NormalChain normalizedChain;

	for(auto i : chain) {
		NormalFrequencies normalizedFreq;

		int totalCount = 0;
		for(auto freqIter : i.second) {
			totalCount += freqIter.second;
		}

		for(auto freqIter : i.second) {
			float norm = float(freqIter.second) / float(totalCount);
			normalizedFreq[freqIter.first] = norm;
		}

		normalizedChain[i.first] = normalizedFreq;
	}

	return normalizedChain;
}

wstring generate(NormalChain &chain, int length) {
	// https://stackoverflow.com/a/17798317/2329879
	random_device rd;
	mt19937 mt(rd());
	uniform_real_distribution<> distr(0, 1);

	wstring output;
	array<wchar_t, ORDER> history;
	history.fill('#');

	for(int i = 0; i < length; i++) {
		wchar_t nextChar;
		NormalFrequencies nextPossible = chain[history];

		double x = distr(mt);
		for(auto charProb : nextPossible) {
			x -= charProb.second;
			if(x <= 0) {
				nextChar = charProb.first;
				break;
			}
		}
		for (int i = 0; i < ORDER - 1; ++i) {
			history[i] = history[i + 1];
		}
		history[ORDER - 1] = nextChar;

		output += nextChar;
	}
	return output;
}

int main(int argc, char const *argv[])
{
	if (argc < 2 || argc > 4) {
		cerr << "usage: " << argv[0] << " file.txt [sentence length] [number of sentences]" << endl;
		return 1;
	}

	int sentenceLength = 1000;
	int numSentences = 2;

	if (argc >= 3) {
		sentenceLength = atoi(argv[2]);
		if (sentenceLength <= 0) {
			cerr << "sentence length must be > 0" << endl;
			return 1;
		}
	}

	if (argc == 4) {
		numSentences = atoi(argv[3]);
		if (numSentences <= 0) {
			cerr << "num sentences must be > 0" << endl;
		}
	}

	auto chain = getChain(string(argv[1]));
	auto nChain = normalize(chain);

	// for(auto i : nChain) {
	// 	for(auto wordChr : i.first) {
	// 		wcout << wordChr;
	// 	}
	// 	wcout << endl;
	// 	for(auto j : i.second) {
	// 		wcout << "\t" << j.first << " " << j.second << endl;
	// 	}
	// }

	for (int i = 0; i < numSentences; i++) {
		wcout << generate(nChain, sentenceLength) << endl;
	}
}